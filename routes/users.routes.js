const express = require("express");
const { signup, login } = require("../controllers/auth.controller");
const {
  getAllUsers,
  updateProfile,
} = require("../controllers/users.controller");
const { auth } = require("../middlewares/auth.middleware");

const userRouter = express.Router();

/**
 * @swagger
 * tags:
 *   name: Users
 *   description: Operations related to users
 */

/**
 * @swagger
 * /signup:
 *   post:
 *     tags:
 *       - Users
 *     summary: Sign up a new user
 *     description: Create a new user account
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *               email:
 *                 type: string
 *               password:
 *                 type: string
 *     responses:
 *       200:
 *         description: User signed up successfully
 *       400:
 *         description: Bad request
 */
userRouter.post("/signup", signup);

/**
 * @swagger
 * /login:
 *   post:
 *     tags:
 *       - Users
 *     summary: Log in a user
 *     description: Authenticate a user with email and password
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               email:
 *                 type: string
 *               password:
 *                 type: string
 *     responses:
 *       200:
 *         description: User logged in successfully
 *       401:
 *         description: Unauthorized
 */
userRouter.post("/login", login);

/**
 * @swagger
 * /getAllUsers:
 *   get:
 *     tags:
 *       - Users
 *     summary: Get all users
 *     description: Retrieve a list of all users
 *     responses:
 *       200:
 *         description: Successfully fetched users
 *       404:
 *         description: Error fetching user data
 */
userRouter.get("/getAllUsers", getAllUsers);

/**
 * @swagger
 * /update/{id}:
 *   patch:
 *     tags:
 *       - Users
 *     summary: Update user profile
 *     description: Update the profile information of a user
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         schema:
 *           type: string
 *           format: uuid
 *         description: The ID of the user to update
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               name:
 *                 type: string
 *               about:
 *                 type: string
 *               tags:
 *                 type: array
 *                 items:
 *                   type: string
 *     responses:
 *       200:
 *         description: Profile updated successfully
 *       404:
 *         description: User not found
 *       500:
 *         description: Internal server error
 */
userRouter.patch("/update/:id", auth, updateProfile);

module.exports = userRouter;
