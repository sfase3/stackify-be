const express = require("express");

const questionRouter = express.Router();


const {
  AskQuestion,
  getAllQuestions,
  deleteQuestion,
  voteQuestion,
} = require("../controllers/question.controller");
const { auth } = require("../middlewares/auth.middleware");

/**
 * @swagger
 * tags:
 *   name: Questions
 *   description: Operations related to questions
 */

/**
 * @swagger
 * /questions/Ask:
 *   post:
 *     tags: 
 *       - Questions
 *     summary: Ask a new question
 *     description: Post a new question to the database
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               title:
 *                 type: string
 *               body:
 *                 type: string
 *               tags:
 *                 type: array
 *                 items:
 *                   type: string
 *     responses:
 *       200:
 *         description: Question posted successfully
 *       409:
 *         description: Conflict - failed to post question
 */
questionRouter.post("/Ask", auth, AskQuestion);

/**
 * @swagger
 * /questions/get:
 *   get:
 *     tags: 
 *       - Questions
 *     summary: Get all questions
 *     description: Retrieve a list of all questions
 *     responses:
 *       200:
 *         description: Successfully fetched questions
 *       500:
 *         description: Internal server error
 */
questionRouter.get("/get", getAllQuestions);

/**
 * @swagger
 * /questions/delete/{id}:
 *   delete:
 *     tags: 
 *       - Questions
 *     summary: Delete a question
 *     description: Delete a question by ID
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         schema:
 *           type: string
 *           format: uuid
 *         description: The ID of the question to delete
 *     responses:
 *       200:
 *         description: Question deleted successfully
 *       404:
 *         description: Question not found
 *       500:
 *         description: Internal server error
 */
questionRouter.delete("/delete/:id", auth, deleteQuestion);

/**
 * @swagger
 * /questions/vote/{id}:
 *   patch:
 *     tags: 
 *       - Questions
 *     summary: Vote on a question
 *     description: Upvote or downvote a question
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         schema:
 *           type: string
 *           format: uuid
 *         description: The ID of the question to vote on
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               value:
 *                 type: string
 *                 enum: [upVote, downVote]
 *               userId:
 *                 type: string
 *     responses:
 *       200:
 *         description: Voted successfully
 *       404:
 *         description: Question not found
 */
questionRouter.patch("/vote/:id", auth, voteQuestion);

module.exports = questionRouter;
