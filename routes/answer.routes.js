const express = require("express");
const answerRouter = express.Router();
const {
  postAnswer,
  deleteAnswer,
} = require("../controllers/answers.controller.js");
const { auth } = require("../middlewares/auth.middleware.js");


/**
 * @swagger
 * /answers/post/{id}:
 *   patch:
 *     summary: Post an answer
 *     description: Add an answer to a question
 *     tags: 
 *       - Answer
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         schema:
 *           type: string
 *           format: uuid
 *         description: The ID of the question
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               noOfAnswers:
 *                 type: integer
 *               answerBody:
 *                 type: string
 *               userAnswered:
 *                 type: string
 *               userId:
 *                 type: string
 *     responses:
 *       200:
 *         description: Answer successfully posted
 *       404:
 *         description: Question not found
 *       400:
 *         description: Bad request
 */
answerRouter.patch("/post/:id", auth, postAnswer);

/**
 * @swagger
 * /answers/delete/{id}:
 *   patch:
 *     summary: Delete an answer
 *     description: Delete an answer from a question
 *     tags: 
 *      - Answer
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         schema:
 *           type: string
 *           format: uuid
 *         description: The ID of the question
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             type: object
 *             properties:
 *               answerId:
 *                 type: string
 *                 format: uuid
 *               noOfAnswers:
 *                 type: integer
 *     responses:
 *       200:
 *         description: Answer successfully deleted
 *       404:
 *         description: Question or answer not found
 *       405:
 *         description: Method not allowed
 */
answerRouter.patch("/delete/:id", auth, deleteAnswer);

module.exports = answerRouter;
