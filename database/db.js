const mongoose = require("mongoose");

const connectDatabase = async () => {
  // Use mongoose to connect
  console.log(process.env.CONNECTION_URL)
 mongoose
    .connect(process.env.CONNECTION_URL)
    // console.log(`Connected to MongoDB server: ${data.connection.host}`);
    .then((data) => {
      console.log(`Connected to MongoDB server: ${data.connection.host}`);
    })
    .catch((error) => {
      console.error("Error connecting to MongoDB:", error);
    });
};

module.exports = connectDatabase;
