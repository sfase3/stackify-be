const express = require("express");
const cors = require("cors");

const userRouter = require("./routes/users.routes.js");
const questionRouter = require("./routes/question.routes.js");
const answerRouter = require("./routes/answer.routes.js");
const swaggerUi = require('swagger-ui-express');
const swaggerJsdoc = require('swagger-jsdoc');

const app = express();

app.use(express.json({ limit: "300mb", extended: true }));
app.use(express.urlencoded({ limit: "30mb", extended: true }));
app.use(cors());

const swaggerOptions = {
  swaggerDefinition: {
    info: {
      title: 'Stackify',
      version: '1.0.0',
      description: 'Docs'
    },
    servers: [
      {
        url: 'http://localhost:3000',
        description: 'Development server'
      }
    ],
    components: {
      securitySchemes: {
        "bearer": {
          "type": "http",
          "scheme": "bearer"
        }
      }
    },
    security: [
      {
        bearerAuth: [],
      },
    ],
  },
  apis: ['./routes/*.js'], 
};

// Инициализация swagger-jsdoc
const swaggerDocs = swaggerJsdoc(swaggerOptions);

// Маршрут для Swagger UI
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocs));

app.get("/", (req, res) => {
  res.send("Stackify");
});

app.use("/user", userRouter);
app.use("/questions", questionRouter);
app.use("/answer", answerRouter);

module.exports = app;
