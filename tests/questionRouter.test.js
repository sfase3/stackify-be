const request = require('supertest');
const express = require('express');
const questionRouter = require('../routes/question.routes');
const { AskQuestion, getAllQuestions, deleteQuestion, voteQuestion } = require('../controllers/question.controller');
const { auth } = require('../middlewares/auth.middleware');

jest.mock('../controllers/question.controller');
jest.mock('../middlewares/auth.middleware');

const app = express();
app.use(express.json());
app.use('/questions', questionRouter);

describe('Question Router', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it('should call AskQuestion controller on POST /Ask', async () => {
    auth.mockImplementation((req, res, next) => next());
    AskQuestion.mockImplementation((req, res) => res.status(201).json({ message: 'Question asked' }));

    const response = await request(app)
      .post('/questions/Ask')
      .set('Authorization', 'Bearer valid_token')
      .send({ title: 'Test Question', content: 'Test Content' });

    expect(auth).toHaveBeenCalled();
    expect(AskQuestion).toHaveBeenCalled();
    expect(response.status).toBe(201);
    expect(response.body).toEqual({ message: 'Question asked' });
  });

  it('should call getAllQuestions controller on GET /get', async () => {
    getAllQuestions.mockImplementation((req, res) => res.status(200).json([{ id: 1, title: 'Test Question' }]));

    const response = await request(app).get('/questions/get');

    expect(getAllQuestions).toHaveBeenCalled();
    expect(response.status).toBe(200);
    expect(response.body).toEqual([{ id: 1, title: 'Test Question' }]);
  });

  it('should call deleteQuestion controller on DELETE /delete/:id', async () => {
    auth.mockImplementation((req, res, next) => next());
    deleteQuestion.mockImplementation((req, res) => res.status(200).json({ message: 'Question deleted' }));

    const response = await request(app)
      .delete('/questions/delete/1')
      .set('Authorization', 'Bearer valid_token');

    expect(auth).toHaveBeenCalled();
    expect(deleteQuestion).toHaveBeenCalled();
    expect(response.status).toBe(200);
    expect(response.body).toEqual({ message: 'Question deleted' });
  });

  it('should call voteQuestion controller on PATCH /vote/:id', async () => {
    auth.mockImplementation((req, res, next) => next());
    voteQuestion.mockImplementation((req, res) => res.status(200).json({ message: 'Question voted' }));

    const response = await request(app)
      .patch('/questions/vote/1')
      .set('Authorization', 'Bearer valid_token')
      .send({ vote: 1 });

    expect(auth).toHaveBeenCalled();
    expect(voteQuestion).toHaveBeenCalled();
    expect(response.status).toBe(200);
    expect(response.body).toEqual({ message: 'Question voted' });
  });
});
